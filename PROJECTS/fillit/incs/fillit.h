/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thgeorge <thgeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/15 11:17:19 by thgeorge          #+#    #+#             */
/*   Updated: 2016/11/30 12:16:35 by thgeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_FILLIT_H
# define FT_FILLIT_H

# include <fcntl.h>
# include "../libft/libft.h"

# define BUFFSZ 1024

/*
**		structs
*/

typedef struct		s_tris
{
	char			**map;
	int				height;
	int				width;
	char			val;
	struct s_tris	*next;
}					t_tris;

typedef struct		s_cord
{
	int				x;
	int				y;
}					t_cord;

typedef struct		s_map
{
	int				sz;
	char			**tab;
}					t_map;

/*
**		Functions Declarations
*/

int					valid_check_chars(char *str);
int					valid_check_pieces(t_tris *lst);
void				valid_check_chars_exeptions(char *c, int i, int *cl,
						int *ln);
int					valid_check_pieces_tab(char **tab);
int					valid_check_pieces_tab_occu(char **tab, int i, int j);

char				*generate_str(int fd);
char				**generate_piece(char *str);
char				**generate_piece_tmp(char *str);
char				**generate_piece_set(char **tmp);
t_tris				*generate_tetras(char *str, int pieces, char val);

char				**tab_new(int h, int w);
t_cord				*cords_new(int x, int y);
t_tris				*piece_new(char **content, char val);
t_map				*map_new(int sz);

t_cord				*tetra_trim_h(char **tetra);
t_cord				*tetra_trim_w(char **tetra);
int					tetra_get_h(char **tetra);
int					tetra_get_w(char **tetra);

void				print_result(t_map *map);

void				free_drop_str(char *str);
void				free_lst(t_tris **lst);
void				free_map(t_map *map);

void				solve_map_main(t_tris *lst, int sz);
int					solve_map_recursive(t_map *map, t_tris *lst);
int					solve_map_test_pos(t_tris *lst, t_map *map, int x, int y);
void				solve_map_set_pos(t_tris *lst, t_map *map,
						t_cord *cords, char c);

#endif
