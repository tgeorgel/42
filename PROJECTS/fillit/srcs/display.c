/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thgeorge <thgeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/17 20:52:36 by thgeorge          #+#    #+#             */
/*   Updated: 2016/11/29 10:21:49 by thgeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/fillit.h"

/*
**		Printing funcs
*/

void	print_result(t_map *map)
{
	int		i;

	i = -1;
	while (map->tab[++i][0])
		ft_putendl(map->tab[i]);
}
