/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   generate.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thgeorge <thgeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/27 15:37:26 by thgeorge          #+#    #+#             */
/*   Updated: 2016/11/30 12:28:47 by thgeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/fillit.h"

/*
**		Generate a temporary 4x4 tab to store our initial tetra
*/

char		**generate_piece_tmp(char *str)
{
	char	**map;
	int		i;
	int		j;

	i = 0;
	j = 0;
	if (!(map = tab_new(4, 4)))
		return (NULL);
	while (*str && i < 4)
	{
		if (*str == '\n')
		{
			map[i][j] = '\0';
			++i;
			j = 0;
		}
		else
			map[i][j++] = *str;
		++str;
	}
	return (map);
}

/*
**		Generate the trimed tetra that will be stored in the list t_tris
*/

char		**generate_piece_set(char **tmp)
{
	char		**map;
	t_cord		*cords_h;
	t_cord		*cords_w;
	int			i;
	int			j;

	cords_h = tetra_trim_h(tmp);
	cords_w = tetra_trim_w(tmp);
	map = tab_new(tetra_get_h(tmp), tetra_get_w(tmp));
	i = -1;
	while (++i < (cords_h->y + 1) - cords_h->x)
	{
		j = -1;
		while (++j < (cords_w->y + 1) - cords_w->x)
			map[i][j] = tmp[i + cords_h->x][j + cords_w->x];
	}
	ft_memdel((void **)&cords_h);
	ft_memdel((void **)&cords_w);
	return (map);
}

/*
**		Calls _piece_tmp and _piece_set
*/

char		**generate_piece(char *str)
{
	char	**map;
	char	**tmp;

	if (!(tmp = generate_piece_tmp(str)))
		return (NULL);
	if (!valid_check_pieces_tab(tmp))
	{
		ft_tabdel(tmp);
		free_drop_str(str);
	}
	map = generate_piece_set(tmp);
	ft_tabdel(tmp);
	return (map);
}

/*
**		Store the tetras into each link of the list
*/

t_tris		*generate_tetras(char *str, int pieces, char val)
{
	t_tris		*new;
	t_tris		*ptr;
	int			i;
	int			counter;
	char		*tmp;

	new = NULL;
	i = -21;
	counter = -1;
	while (str[i += 21] && ++counter < pieces)
	{
		tmp = ft_strsub(str, i, 20);
		if (!new)
		{
			new = piece_new(generate_piece(tmp), val++);
			ptr = new;
		}
		else
		{
			new->next = piece_new(generate_piece(tmp), val++);
			new = new->next;
		}
		ft_strdel(&tmp);
	}
	return (ptr);
}

/*
**		Read a file and return the content in a string
*/

char		*generate_str(int fd)
{
	char	buf[BUFFSZ];
	int		ret;

	ret = read(fd, buf, BUFFSZ);
	buf[ret] = '\0';
	return (ft_strdup(buf));
}
