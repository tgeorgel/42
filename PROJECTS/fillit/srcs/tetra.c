/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tetra.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thgeorge <thgeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 15:20:40 by thgeorge          #+#    #+#             */
/*   Updated: 2016/11/27 16:18:10 by thgeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/fillit.h"

/*
**		Trim the empty lines of the tetra
*/

t_cord		*tetra_trim_h(char **tetra)
{
	int			i;
	int			j;
	int			mn;
	int			mx;
	int			c;

	i = -1;
	mn = -1;
	while (++i < 4)
	{
		j = -1;
		c = 0;
		while (++j < 4)
		{
			if (tetra[i][j] == '#')
				c = 1;
			if (c == 1 && (mn == -1))
				mn = i;
			if (c == 1)
				mx = i;
		}
	}
	return (cords_new(mn, mx));
}

t_cord		*tetra_trim_w(char **tetra)
{
	int			i;
	int			j;
	int			mn;
	int			mx;
	int			c;

	j = -1;
	mn = -1;
	while (++j < 4)
	{
		i = -1;
		c = 0;
		while (++i < 4)
		{
			if (tetra[i][j] == '#')
				c = 1;
			if (c == 1 && (mn == -1))
				mn = j;
			if (c == 1)
				mx = j;
		}
	}
	return (cords_new(mn, mx));
}

/*
**		Get the Height and Width of the final tetra
*/

int			tetra_get_h(char **tetra)
{
	t_cord		*cords_h;
	int			ret;

	cords_h = tetra_trim_h(tetra);
	ret = (cords_h->y + 1) - cords_h->x;
	ft_memdel((void **)&cords_h);
	return (ret);
}

int			tetra_get_w(char **tetra)
{
	t_cord		*cords_w;
	int			ret;

	cords_w = tetra_trim_w(tetra);
	ret = (cords_w->y + 1) - cords_w->x;
	ft_memdel((void **)&cords_w);
	return (ret);
}
