/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thgeorge <thgeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/19 10:58:53 by thgeorge          #+#    #+#             */
/*   Updated: 2016/11/27 16:32:44 by thgeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/fillit.h"

/*
**		Set the piece in the map
*/

void		solve_map_set_pos(t_tris *lst, t_map *map, t_cord *cords, char c)
{
	int		i;
	int		j;

	j = -1;
	while (++j < lst->width)
	{
		i = -1;
		while (++i < lst->height)
			if (lst->map[i][j] == '#')
				map->tab[cords->x + i][cords->y + j] = c;
	}
	ft_memdel((void **)&cords);
}

/*
**		Test if the piece can fit in the map at the cords X,Y
*/

int			solve_map_test_pos(t_tris *lst, t_map *map, int x, int y)
{
	int		i;
	int		j;

	j = -1;
	while (++j < lst->width)
	{
		i = -1;
		while (++i < lst->height)
			if (lst->map[i][j] == '#' && map->tab[x + i][y + j] != '.')
				return (0);
	}
	solve_map_set_pos(lst, map, cords_new(x, y), lst->val);
	return (1);
}

/*
**		Recursive function. It test all positions in the map to put the actual
**		piece. If it fits (solve_map_test_pos) then we put the piece and get
**		to the next piece. If one piece can't be placed, then we simply get
**		back to the previous one, remove it and test pos + 1. if the map size
**		is too small, the function will end up returning 0.
*/

int			solve_map_recursive(t_map *map, t_tris *lst)
{
	int		x;
	int		y;

	if (lst == NULL)
		return (1);
	x = -1;
	while (++x <= map->sz - lst->height)
	{
		y = -1;
		while (++y <= map->sz - lst->width)
		{
			if (solve_map_test_pos(lst, map, x, y))
			{
				if (solve_map_recursive(map, lst->next))
					return (1);
				else
					solve_map_set_pos(lst, map, cords_new(x, y), '.');
			}
		}
	}
	return (0);
}

/*
**		Call the recursive function, if it fails call it again with size + 1
*/

void		solve_map_main(t_tris *lst, int sz)
{
	t_map	*map;

	map = map_new(sz);
	while (!solve_map_recursive(map, lst))
	{
		++sz;
		free_map(map);
		map = map_new(sz);
	}
	print_result(map);
	free_map(map);
}
