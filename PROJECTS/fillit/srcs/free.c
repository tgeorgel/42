/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thgeorge <thgeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/27 15:34:59 by thgeorge          #+#    #+#             */
/*   Updated: 2016/11/27 16:12:23 by thgeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/fillit.h"

/*
**		Free the elements
*/

void	free_lst(t_tris **lst)
{
	t_tris		*cpy;
	t_tris		*tmp;

	cpy = *lst;
	while (cpy)
	{
		tmp = cpy->next;
		ft_tabdel(cpy->map);
		free(cpy);
		cpy = tmp;
	}
	*lst = NULL;
}

void	free_map(t_map *map)
{
	ft_tabdel(map->tab);
	free(map);
}

void	free_drop_str(char *str)
{
	ft_strdel(&str);
	ft_throw("error", 1);
}
