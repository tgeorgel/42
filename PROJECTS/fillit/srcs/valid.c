/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   valid.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thgeorge <thgeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/15 12:56:45 by thgeorge          #+#    #+#             */
/*   Updated: 2016/11/30 12:56:23 by thgeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/fillit.h"

/*
**		Check the occurences of '#' (neighbours method)
*/

int		valid_check_pieces_tab_occu(char **tab, int i, int j)
{
	int		nb;

	nb = 0;
	if (j != 0 && tab[i][j - 1] == '#')
		++nb;
	if (tab[i][j + 1] == '#')
		++nb;
	if (i != 0 && tab[i - 1][j] == '#')
		++nb;
	if (tab[i + 1][j] == '#')
		++nb;
	return (nb);
}

/*
**		Count '#' and occurencies (with valid_check_pieces_tab_occu)
*/

int		valid_check_pieces_tab(char **tab)
{
	int		i;
	int		j;
	int		nb;
	int		occu;

	i = -1;
	nb = 0;
	occu = 0;
	while (tab[++i][0])
	{
		j = -1;
		while (tab[i][++j])
			if (tab[i][j] == '#')
			{
				++nb;
				occu += valid_check_pieces_tab_occu(tab, i, j);
			}
	}
	if (nb == 4 && (occu == 6 || occu == 8))
		return (1);
	return (0);
}

/*
**		Verify the char so it is '.', '#', or '\n'
*/

void	valid_check_chars_exeptions(char *c, int i, int *cl, int *ln)
{
	if (c[i] == '\n')
		++(*ln);
	if (c[i] != '.' && c[i] != '#' && c[i] != '\n')
		ft_throw("error", 1);
	if (c[i] == '\n' && *cl != 5)
		ft_throw("error", 1);
	if (*ln == 4 && c[i + 1] != '\0' && c[i + 2] == '\0')
		ft_throw("error", 1);
	if (*ln == 4 && c[i + 1] != '\0' && c[i + 1] != '\n')
		ft_throw("error", 1);
	if (c[i] == '\n')
		*cl = 0;
}

/*
**		Check Chars (apply valid_check_chars_exeptions) and return nb of pieces
*/

int		valid_check_chars(char *str)
{
	int		ln;
	int		cl;
	int		pieces;
	int		i;

	ln = 0;
	cl = 0;
	pieces = 0;
	i = -1;
	while (str[++i] && ++cl < 7)
	{
		valid_check_chars_exeptions(str, i, &cl, &ln);
		if (ln == 4)
		{
			if (str[i + 1] == '\n')
				++i;
			++pieces;
			ln = 0;
		}
	}
	if (ln == 0 && cl == 0)
		return (pieces);
	return (0);
}
