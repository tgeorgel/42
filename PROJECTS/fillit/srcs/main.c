/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thgeorge <thgeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/15 11:45:43 by thgeorge          #+#    #+#             */
/*   Updated: 2016/11/30 12:56:13 by thgeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/fillit.h"

int		main(int ac, char **av)
{
	int			fd;
	int			pieces;
	char		*str;
	t_tris		*lst;

	if (ac != 2)
		ft_throw("usage: ./fillit source_file", 2);
	if ((fd = open(av[1], O_RDONLY)) == -1)
		ft_throw("error", 1);
	str = generate_str(fd);
	if (!ft_strlen(str) || (close(fd) == -1))
		free_drop_str(str);
	if (!(pieces = valid_check_chars(str)))
		free_drop_str(str);
	lst = generate_tetras(str, pieces, 'A');
	ft_strdel(&str);
	solve_map_main(lst, ft_upsqrt(pieces * 4));
	free_lst(&lst);
	return (0);
}
