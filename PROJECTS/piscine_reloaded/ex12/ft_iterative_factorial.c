/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thgeorge <thgeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/02 21:41:15 by thgeorge          #+#    #+#             */
/*   Updated: 2016/11/04 13:38:49 by thgeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_iterative_factorial(int nb)
{
	int		i;
	int		r;

	i = 0;
	r = 1;
	if (nb < 0 || nb > 12)
		return (0);
	while (++i <= nb)
		r *= i;
	return (r);
}
