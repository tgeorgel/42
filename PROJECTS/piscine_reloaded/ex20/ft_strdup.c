/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thgeorge <thgeorge@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/03 03:28:19 by thgeorge          #+#    #+#             */
/*   Updated: 2016/11/03 04:18:13 by thgeorge         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_strlen(char *str)
{
	int		i;

	i = 0;
	while (str[i++])
		continue;
	return (i);
}

char	*ft_strdup(char *src)
{
	char	*cpy;
	int		i;

	if ((cpy = (char *)malloc(sizeof(char) * (ft_strlen(src) + 1))))
	{
		i = -1;
		while (src[++i])
			cpy[i] = src[i];
		cpy[i] = '\0';
		return (char *)(cpy);
	}
	return (0);
}
