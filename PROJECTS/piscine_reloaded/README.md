# 42/projects/piscine_reloaded


##### (Francais)

Ce projet est le premier projet de l'école 42. Il s'agit d'un rappel des notions de piscine, l'epreuve d'entrée

Sujet dispo ici :
https://gitlab.com/TheTrueElektro/42/tree/master/SUBJECTS/GENERAL/

###### Attention, cette libft est celle du projet, ma libft à jour est dispo dans mon depo : https://gitlab.com/TheTrueElektro/42/tree/master/FILES/LIBFT/ .




##### (English)

This projects is the first project in 42 school. The goal is to rework on the notions viewed in the piscine, the acceptation test

Subject available here :
https://gitlab.com/TheTrueElektro/42/tree/master/SUBJECTS/GENERAL/

###### Be carefull, this is the libft project, my actual libft can be found in my repo : https://gitlab.com/TheTrueElektro/42/tree/master/FILES/LIBFT/ .
